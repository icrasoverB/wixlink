<?php
class Api extends CI_Model{


	public function __Construct(){
		parent::__Construct();
	}

  public function listar($code){
    $this->db->where('code',$code);
    $this->db->where('status', 0);
    $query = $this->db->get('wp_wixsms_sms');
    if($query->num_rows() > 0)
      return $query->result();
    else
      return false;

  }

  public function statusTarea($idUsuario){
    $this->db->select('status');
    $this->db->where('id_usuario', $idUsuario);
    $query = $this->db->get('wp_wixsms_tarea');
    if($query->num_rows() > 0)
      return $query->row()->status;
    else
      return false;

  }

  public function statusSms($idsms){
    $this->db->where('status',0);
    $this->db->where('idsms', $idsms);
    $query = $this->db->get('wp_wixsms_sms');
    if($query->num_rows() > 0)
      return true;
    else
      return false;

  }

  public function actualizarTarea($status,$idUsuario){
      $datos = array('status'=>$status);
      $this->db->where('id_usuario', $idUsuario);
      $query = $this->db->update('wp_wixsms_tarea', $datos);
      if($this->db->affected_rows()>0){
        return true;
      }
      else{
        return false;
      }
}
  public function actualizarStatusSms($idUsuario){
      $datos = array('status'=>1);
      $this->db->where('idsms', $idUsuario);
      $query = $this->db->update('wp_wixsms_sms', $datos);
      if($this->db->affected_rows()>0){
        return true;
      }
      else{
        return false;
      }

  }


  public function validarToken($token){
    $this->db->select('email');
    $this->db->where('facebook_token', $token);
    $query = $this->db->get('wp_wixsms_suscrito');
    if($query->num_rows() > 0)
      return true;
    else
      return false;
  }

  public function addMensaje($datos){
      $query = $this->db->insert('wp_wixsms_sms', $datos);
      if($this->db->affected_rows() > 0){
        return true;
      }
      else{
        return false;
      }
  }


}
