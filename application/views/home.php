<?php $this->load->view('header');?>
<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->

<div class="container">
		<div class="row justify-content-md-center page-header">
				<div class="col-sm-12 col-xs-12 col-lg-6 col-md-12">
					<div class="panel panel-danger box-danger">
							<div class="panel-body text-center">
									<p class="titu">Por favor, marque el cuadro de captcha para ir a la página de destino.</p>
									<div class="p300x250">
										<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
									<!-- p_300x250_alcaldia -->
									<ins class="adsbygoogle"
									     style="display:inline-block;width:300px;height:250px"
									     data-ad-client="ca-pub-7467403016811498"
									     data-ad-slot="4286088360"></ins>
									<script>
									(adsbygoogle = window.adsbygoogle || []).push({});
									</script>
									</div>
									<form action="#" id="skip" method="post" accept-charset="utf-8">
									<div id="continue" class="center-captcha">
											<!--<div id="captcha" class="g-recaptcha" data-callback="correctCaptcha" data-sitekey="6LeC9xUUAAAAAC7HHNTJup-f4CxuKWrXi1J4fYKV"></div>-->
									</div>
								</form>
							</div>
					</div>
				</div>
		</div>
</div>

<div class="container">
		<div class="row justify-content-md-center info">
				<div class="col-sm-12 col-xs-12 col-lg-6 col-md-12">
						<hr>
						<br>
						<h3>Qué es Wixink.org?</h3>
						<p>
							Wixink.org es un servicio de reducción de URL que permite a los usuarios recibir pagos cuando comparten enlaces y alguien hace clic. Pagamos por TODOS los visitantes legítimos que aportas a tus enlaces y pagas al menos $ 1.6 por cada 1.000 visitas. Múltiples vistas desde el mismo espectador también se cuentan por lo que se beneficiará de todo su tráfico.
						</p>
						<hr>
						<div class="p728x90">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<!-- p728x90_alcaldia -->
						<ins class="adsbygoogle"
						     style="display:inline-block;width:728px;height:90px"
						     data-ad-client="ca-pub-7467403016811498"
						     data-ad-slot="2948955969"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
						</div>
						<br>
						<h3>Acortar urls y gana dinero</h3>
						<p>
							Regístrate para una cuenta en sólo 2 minutos. Una vez que haya completado su registro, comience a crear URL cortas y compartir los vínculos con su familia y amigos.
						</p>
						<div class="p320x60">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<!-- p320x60 -->
							<ins class="adsbygoogle"
							     style="display:block"
							     data-ad-client="ca-pub-7467403016811498"
							     data-ad-slot="7967911567"
							     data-ad-format="auto"></ins>
							<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>
				</div>
		</div>
</div>

<?php $this->load->view('footer');?>
